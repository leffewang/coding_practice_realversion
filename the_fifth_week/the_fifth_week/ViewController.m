//
//  ViewController.m
//  the_fifth_week
//
//  Created by 王峥 on 2019/6/17.
//  Copyright © 2019 futu. All rights reserved.
//

#import "ViewController.h"
#import "Myinteger.h"

@interface ViewController()
@property (weak) IBOutlet NSTextField *sayhi;

@end
@implementation ViewController

#pragma mark - Overridden(Life Cycle)
- (void)viewDidLoad {
    [super viewDidLoad];
    self.sayhi.editable = YES;
    SEL hihi = @selector(run);
    NSLog(@"Selector: %s",sel_getName(hihi));
    
    Myinteger *cris = [Myinteger new];
    int a = 0,b = 0;
    NSString *duke = @"啊啊啊啊";
    cris.intValue = 42;
    NSLog(@"第一次尝试上");
    for (int i = 0; i < 100000; i++){
        a += [cris intValue];
    }
    NSLog(@"第一次尝试下 = %d",a);
    SEL ImFineWithIMP = @selector(hurry:);
    IMP intValuespecial = [self methodForSelector:@selector(specialCase)];//注释ImFineWithIMP //
    intValuespecial();
    NSLog(@"第二次尝试上%d",(int)intValuespecial);
    for (int i = 0; i < 100000; i++){
        b += (int) intValuespecial;
    }
    NSLog(@"第二次尝试下 = %d", b);
    [self performSelector:@selector(run)];
}


- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}

#pragma mark - Private Method

- (IBAction)caculate:(id)sender {
    [self run];
    
}
- (IBAction)superCaculate:(id)sender {
    [self superRun];
}

- (void)run{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        int i,k;
        NSNumber *jermaine = @(0);
        NSLog(@"%@",jermaine);
        for (i = 0; i <1000000; i++){
            jermaine = @(0);
            for (k = 0; k < 1000; k++){
                jermaine = @([jermaine intValue] + k);
            }
        }
        NSLog(@"%p",jermaine);
        dispatch_async(dispatch_get_main_queue(), ^{
            self.sayhi.cell.title = (NSString *) jermaine;
        });
    });
}

- (void)superRun{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        int i, k;
        int sum = 0;
        NSLog(@"%@", [NSString stringWithFormat:@"%d", sum]);
        for (i = 0; i <1000000; i++){
            sum = 0;
            for (k = 0; k < 1000; k++){
                sum += k;
            }
        }
        NSLog(@"%d",sum);
        dispatch_async(dispatch_get_main_queue(), ^{
            self.sayhi.cell.title = [NSString stringWithFormat:@"%d",sum];
        });
    });
    
}

- (void)hurry: (NSString *)myFriend{
    NSLog(@"这边的朋友你们好%@",myFriend);
}

- (void)specialCase{
    NSLog(@"hi");
}

@end
