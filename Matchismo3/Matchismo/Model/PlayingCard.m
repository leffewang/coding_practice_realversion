//
//  PlayingCard.m
//  Matchismo
//
//  Created by 王峥 on 2019/5/22.
//  Copyright © 2019 futu. All rights reserved.
//

#import "PlayingCard.h"

@implementation PlayingCard
- (NSString*)contents{
    NSArray *rankString = [PlayingCard rankStrings];
    return [rankString[self.rank] stringByAppendingString:self.suit];
}

+ (NSArray *)validSuits{
    NSArray *suits = [[NSArray alloc]init];
    suits = @[@"♡",@"♤",@"♧",@"♢"];
    return suits;
}

+ (NSUInteger)maxRank{
    return [[self rankStrings] count] -1;
}

+ (NSArray *) rankStrings{
    NSArray *rankstring = [[NSArray alloc]init];
    rankstring = @[@"?",@"A",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"J",@"Q",@"K"];
    return rankstring;
}

@synthesize suit = _suit;

- (NSString *)suit{
    return _suit ? _suit: @"?";
}

- (void)setSuit:(NSString *)suit
{
    if([[PlayingCard validSuits] containsObject:suit]){
        _suit = suit;
    }
}

- (void) setRank:(NSUInteger)rank
{
    if(rank <= [PlayingCard maxRank]){
        _rank = rank;
    }
}

-(int)match:(NSArray *)otherCards{
    int score = 0;
    if ([otherCards count] == 1){
        id  otherCard = [otherCards firstObject];
        if ([otherCard isKindOfClass:[PlayingCard class]]){
            PlayingCard * otherCardRight = (PlayingCard *)otherCard;
            if (otherCardRight.rank == self.rank){
                score = 4;
            }
            else if (otherCardRight.suit == self.suit){
                score = 1;
            }
        }
    }
    else if ([otherCards count] == 2){
        id  firstCard = [otherCards firstObject];
        id  secondCard = [otherCards objectAtIndex:1];
        if ([firstCard isKindOfClass:[PlayingCard class]] && [secondCard isKindOfClass:[PlayingCard class]]){
            PlayingCard * firstCardRight = (PlayingCard *)firstCard;
            PlayingCard * secondCardRight = (PlayingCard *)secondCard;
            if (firstCardRight.rank == self.rank && self.rank == secondCardRight.rank){
                score = 4;
            }
            else if (firstCardRight.suit == self.suit && self.suit == secondCardRight.suit){
                score = 1;
            }
        }
    }
    return score;
}
@end
