//
//  Card.m
//  Matchismo
//
//  Created by 王峥 on 2019/5/22.
//  Copyright © 2019 futu. All rights reserved.
//

#import "Card.h"

@implementation Card

-(int)match: (NSArray *)otherCards{
    int score = 0;
    for( Card *card in otherCards){
        if([card.contents isEqualToString:self.contents]){
            score = 1;
        }
    }
    return score;
}
@end
