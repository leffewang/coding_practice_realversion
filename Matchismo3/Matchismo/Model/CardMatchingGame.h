//
//  CardMatchingGame.h
//  Matchismo
//
//  Created by 王峥 on 2019/5/23.
//  Copyright © 2019 futu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Deck.h"

NS_ASSUME_NONNULL_BEGIN

@interface CardMatchingGame : NSObject

-(instancetype)initWithCardCount:(NSUInteger) count
                       usingDeck:(Deck *)deck;

-(void) chooseCardAtIndex: (NSUInteger)index;
-(void) chooseCardAtIndexInThreeCardMode:(NSUInteger)index;

-(Card *)cardAtIndex: (NSUInteger)index;

@property (nonatomic, readonly) NSInteger score;
@end

NS_ASSUME_NONNULL_END
