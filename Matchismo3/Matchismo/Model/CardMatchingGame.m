//
//  CardMatchingGame.m
//  Matchismo
//
//  Created by 王峥 on 2019/5/23.
//  Copyright © 2019 futu. All rights reserved.
//

#import "CardMatchingGame.h"
@interface CardMatchingGame()
@property (nonatomic,readwrite) NSInteger score;
@property (nonatomic, strong) NSMutableArray *cards;
@property (nonatomic, strong) NSMutableArray *buffer;
@property (nonatomic) NSUInteger thefirstcard;
@end

@implementation CardMatchingGame

-(NSMutableArray *)cards{
    if(!_cards) _cards = [[NSMutableArray alloc]init];
    return _cards;
}

-(NSMutableArray *)buffer{
    if(!_buffer) _buffer = [[NSMutableArray alloc]init];
    return _buffer;
}

-(instancetype) initWithCardCount:(NSUInteger)count usingDeck:(Deck *)deck{
    self = [super init];
    if(self){
        for (int i = 0; i < count; i++) {
            Card *card = [deck drawRandomCard];
            if (card){
                [self.cards addObject:card];
            }else{
                self = nil;
                break;
            }
        }
    }
    
    return  self;
}

- (Card *)cardAtIndex:(NSUInteger)index{
    return (index < [self.cards count]) ? self.cards[index] : nil;
}


static const int MATCH_BONUS = 4;
static const int MISMATCH_PENALTY = 1;
static const int COST_TO_CHOOSE = 1;

-(void)chooseCardAtIndex:(NSUInteger)index{
    Card *card = [self cardAtIndex:index];
    
    if(!card.isMatched){
        if(card.isChosen){
            card.chosen = NO;
        }else{
            for (Card *otherCard in self.cards){
                if(otherCard.isChosen && !otherCard.isMatched){
                    int matchScore = [card match:@[otherCard]];
                    if (matchScore){
                        self.score += matchScore * MATCH_BONUS;
                        otherCard.matched = YES;
                        card.matched = YES;
                    }else{
                        self.score -= MISMATCH_PENALTY;
                        otherCard.chosen = NO;
                    }
                    break; // can only choose two cards now
                }
            }
            self.score -= COST_TO_CHOOSE;
            card.chosen = YES;
        }
    }
}

-(void)chooseCardAtIndexInThreeCardMode:(NSUInteger)index{
    Card *card = [self cardAtIndex:index];
    self.buffer =  [[NSMutableArray alloc]init];
    if(!card.isMatched){
        if(card.isChosen){
            card.chosen = NO;
        }else{
            for (Card *otherCard in self.cards){
                if(otherCard.isChosen && !otherCard.isMatched){
                    [self.buffer addObject:otherCard];
                    if([self.buffer count] == 1){
                        self.thefirstcard = [self.cards indexOfObject:otherCard];
                    }
                    else if([self.buffer count] == 2){
                        int matchScore = [card match:self.buffer];
                        if (matchScore){
                            self.score += matchScore * MATCH_BONUS;
                            otherCard.matched = YES;
                            Card * Fristcard =[self.cards objectAtIndex:self.thefirstcard];
                            Fristcard.matched = YES;
                            card.matched = YES;
                        }else{
                            otherCard.chosen = NO;
                        }
                        break;
                    }
                }
            }
            self.score -= COST_TO_CHOOSE;
            card.chosen = YES;
        }
    }
}


@end
