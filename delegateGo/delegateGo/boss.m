//
//  boss.m
//  delegateGo
//
//  Created by 王峥 on 2019/7/1.
//  Copyright © 2019 futu. All rights reserved.
//

#import "boss.h"

@implementation boss

- (NSString *)power{
    if(!_power){
        _power = [[NSString alloc]initWithFormat:@"Rua"];
    }
    return _power;
}

- (void)givePowerToFellow{
    if([self.delegate respondsToSelector:@selector(geShanDaNiu:)]){
        [self.delegate geShanDaNiu:self.power];
    }
}

@end
