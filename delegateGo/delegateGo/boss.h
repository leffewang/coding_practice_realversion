//
//  boss.h
//  delegateGo
//
//  Created by 王峥 on 2019/7/1.
//  Copyright © 2019 futu. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol winterIsCome;

@interface boss : NSObject

@property (nonatomic, strong) NSString *power;
@property (nonatomic, weak) id <winterIsCome> delegate;

- (void) givePowerToFellow;

@end

@protocol winterIsCome <NSObject>

- (void) geShanDaNiu: (NSString *) hoho;
@end


