//
//  ViewController.m
//  delegateGo
//
//  Created by 王峥 on 2019/7/1.
//  Copyright © 2019 futu. All rights reserved.
//

#import "ViewController.h"
#import "assistant.h"

@interface ViewController ()

@property (nonatomic, strong) boss *duke;
@property (nonatomic, strong) assistant *jermaine;
@end

@implementation ViewController

- (boss *)duke{
    if(!_duke){
        _duke = [[boss alloc]init];
    }
    return _duke;
}

- (assistant *)jermaine{
    if(!_jermaine){
        _jermaine = [[assistant alloc]init];
    }
    return _jermaine;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.duke.delegate = self.jermaine;
    [self.duke givePowerToFellow];
    // Do any additional setup after loading the view.
}


- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}


@end
