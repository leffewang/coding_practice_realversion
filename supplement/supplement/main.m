//
//  main.m
//  supplement
//
//  Created by 王峥 on 2019/6/20.
//  Copyright © 2019 futu. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
