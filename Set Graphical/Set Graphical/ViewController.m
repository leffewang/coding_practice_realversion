//
//  ViewController.m
//  Set Graphical
//
//  Created by 王峥 on 2019/5/30.
//  Copyright © 2019 futu. All rights reserved.
//

#import "ViewController.h"
@interface ViewController()
#pragma mark - Private Property
@property (nonatomic, weak) IBOutlet NSView *gameView;
@property (nonatomic, strong) NSLock *lock;
@property (nonatomic) NSUInteger hey;
@end
@implementation ViewController

#pragma mark - Overridden(Life Cycle)
- (NSUInteger)hey{
    if (!_hey){
        _hey = 0;
    }
    return _hey;
}



#pragma mark - Overridden(Life Cycle)

- (void) awakeFromNib{
    
}


- (void) viewDidLoad {
    [super viewDidLoad];
    NSURL *duke = [NSURL URLWithString:[NSString stringWithFormat:@"www.baidu.com"]];
    for (int i = 0; i < 50; i++){
        NSLog(@"我是一梯队%@号进程，我输出了%d", [NSThread currentThread], i);
        if (i == 20){
            [NSThread detachNewThreadSelector:@selector(run) toTarget:self withObject:nil];
            NSThread *jermaine = [[NSThread alloc]initWithTarget:self selector:@selector(run) object:nil];
            [jermaine start];
        }
    }
    
    dispatch_queue_t jermiane = dispatch_queue_create("sdkfao.queue", DISPATCH_QUEUE_CONCURRENT);
    dispatch_async(jermiane, ^(void){
        for (int i = 0; i < 50; i++){
            NSLog(@"我是三梯队%@号进程，我输出了%d", [NSThread currentThread], i);
        }
        NSXMLParser *jermaine = [[NSXMLParser alloc]initWithContentsOfURL:duke];
    });
    


    // Do any additional setup after loading the view.
}



- (void) setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}

#pragma mark - Private Method
static CGSize const CSDropSize = { 40, 40 };

- (IBAction) gameControl:(NSButton *)sender {
    [self dropBlock];
}


- (void) dropBlock{
    __block int i = (int)self.hey;
    dispatch_async (dispatch_get_global_queue (DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^ (void) {
        i++;
        NSLog(@"%@----%d",[NSThread currentThread],i);
        dispatch_async(dispatch_get_main_queue(), ^{
            self.hey = i;
        });
    });
    [self.gameView isFlipped];
    CGRect frame;
    frame.origin = CGPointZero;
    frame.size = CSDropSize;
    int dropLocationX = (arc4random() % (int)self.gameView.bounds.size.width) / CSDropSize.width;
    int dropLocationY = (arc4random() % (int)self.gameView.bounds.size.height) / CSDropSize.height;
    frame.origin.x = dropLocationX * CSDropSize.width;
    frame.origin.y = dropLocationY * CSDropSize.width;
    NSView *dropView = [[NSView alloc]initWithFrame:frame];
    [dropView isFlipped];
    dropView.wantsLayer = YES;
    dropView.layer.backgroundColor = [self randomColor].CGColor;
    [self.gameView addSubview:dropView];
    
    
}

- (NSColor *) randomColor{
    switch (arc4random() % 5) {
        case 0:
            return [NSColor redColor];
        case 1:
            return [NSColor blueColor];
        case 2:
            return [NSColor orangeColor];
        case 3:
            return [NSColor yellowColor];
        case 4:
            return [NSColor greenColor];
    }
    return [NSColor systemPinkColor];
}

- (IBAction) windowCreate:(NSButton *)sender {
    NSRect frame = CGRectMake(0, 0, 200, 200);
    NSUInteger style =  NSWindowStyleMaskTitled | NSWindowStyleMaskClosable |NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable;
    NSWindow *window = [[NSWindow alloc]initWithContentRect:frame styleMask:style backing:NSBackingStoreBuffered defer:YES];
    window.title = @"I'm comming my friend";
    [window makeKeyAndOrderFront:self];
    [window center];
    NSButton *forExit = [[NSButton alloc]initWithFrame:CGRectMake(10, 10, 100, 30)];
    [window.contentView addSubview:forExit];
    forExit.target = self;
    forExit.action = @selector(codeButtonClicked:);
    NSBox *horizontalSeparator=[[NSBox alloc]
                                 initWithFrame:NSMakeRect(15.0,250.0,250.0,1.0)];
    [horizontalSeparator setBoxType:NSBoxSeparator];
    [window.contentView addSubview:horizontalSeparator];
    NSBox *box=[[NSBox alloc]
                 initWithFrame:NSMakeRect(15.0,200.0,160.0,100)];
    [box setBoxType:NSBoxPrimary];
    NSSize margin = NSMakeSize(20, 20);
    box.contentViewMargins = margin;
    
    NSTextField *textField = [[ NSTextField alloc]
                              initWithFrame:NSMakeRect(10,10,80,20)];
    [box.contentView addSubview:textField];
    NSURL *rock = [[NSBundle mainBundle] URLForResource:@"混声CD1" withExtension:@"mp4"];
    [window.contentView addSubview:box];
}

- (IBAction) codeButtonClicked:(id)sender{
    NSLog(@"hi");
    
}

- (void) windowWillClose:(NSNotification *)notification {
    [[NSApplication sharedApplication]stopModal];
}

- (void) run{
    for ( int i = 0; i < 40; i++){
        NSLog(@"我是二梯队%@号进程，我输出了%d，我的能量值为%f", [NSThread currentThread], i, [NSThread threadPriority]);
    }
}

@end
