//
//  PlayingCard.h
//  Matchismo
//
//  Created by 王峥 on 2019/5/22.
//  Copyright © 2019 futu. All rights reserved.
//

#import "Card.h"

/*The parent class of Playing card is Card*/
@interface PlayingCard : Card
/*The suit means the type of card*/
@property (strong, nonatomic) NSString *suit;
/*The rank means the value of card*/
@property (nonatomic) NSUInteger rank;

/*return an array comprise the valid suits*/
+ (NSArray *)validSuits;
/*return an interger which is the max rank*/
+ (NSUInteger)maxRank;
@end

