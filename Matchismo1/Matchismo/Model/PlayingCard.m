//
//  PlayingCard.m
//  Matchismo
//
//  Created by 王峥 on 2019/5/22.
//  Copyright © 2019 futu. All rights reserved.
//

#import "PlayingCard.h"

@implementation PlayingCard
- (NSString*)contents{
    NSArray *rankString = [PlayingCard rankStrings];
    return [rankString[self.rank] stringByAppendingString:self.suit];
}

+ (NSArray *)validSuits{
    NSArray *suits = [[NSArray alloc]init];
    suits = @[@"♡",@"♤",@"♧",@"♢"];
    return suits;
}

+ (NSUInteger)maxRank{
    return [[self rankStrings] count] -1;
}

+ (NSArray *) rankStrings{
    NSArray *rankstring = [[NSArray alloc]init];
    rankstring = @[@"?",@"A",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"J",@"Q",@"K"];
    return rankstring;
}

@synthesize suit = _suit;

- (NSString *)suit{
    return _suit ? _suit: @"?";
}

- (void)setSuit:(NSString *)suit
{
    if([[PlayingCard validSuits] containsObject:suit]){
        _suit = suit;
    }
}

- (void) setRank:(NSUInteger)rank
{
    if(rank <= [PlayingCard maxRank]){
        _rank = rank;
    }
}
@end
