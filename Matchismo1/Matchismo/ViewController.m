//
//  ViewController.m
//  Matchismo
//
//  Created by 王峥 on 2019/5/22.
//  Copyright © 2019 futu. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *flipsLabel;
@property (strong, nonatomic) Deck * cardlist;
@property (nonatomic) int flipCount;
@end

@implementation ViewController

- (Deck *)cardlist{
    if(!_cardlist) _cardlist = [[PlayingCardDeck alloc] init];
    return _cardlist;
}
-(void)setFlipCount:(int)flipCount{
    _flipCount = flipCount;
    self.flipsLabel.text = [NSString stringWithFormat:@"Flips: %d", self.flipCount];
}
- (IBAction)touchCardButton:(UIButton *)sender
{
    /*Change to the hind of card if there existing contents in current title*/
    if([sender.currentTitle length]){
        [sender setBackgroundImage: [UIImage imageNamed: @"cardback"]
                          forState: UIControlStateNormal];
        [sender setTitle:@"" forState:UIControlStateNormal];
        
    }else{
        Card *chooseOne = [self.cardlist drawRandomCard];
        NSLog(@"%@",chooseOne.contents);
        [sender setBackgroundImage:[UIImage imageNamed:@"cardfront"]
                          forState:UIControlStateNormal];
        [sender setTitle:chooseOne.contents forState:UIControlStateNormal];
    }
    self.flipCount ++;
}



@end
