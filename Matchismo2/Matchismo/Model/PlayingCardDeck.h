//
//  PlayingCardDeck.h
//  Matchismo
//
//  Created by 王峥 on 2019/5/22.
//  Copyright © 2019 futu. All rights reserved.
//

#import "Deck.h"
#import "PlayingCard.h"

NS_ASSUME_NONNULL_BEGIN

@interface PlayingCardDeck : Deck

@end

NS_ASSUME_NONNULL_END
