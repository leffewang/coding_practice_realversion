//
//  main.m
//  网络请求练习
//
//  Created by 王峥 on 2019/6/26.
//  Copyright © 2019 futu. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
