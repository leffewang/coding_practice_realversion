//
//  ViewController.m
//  网络请求练习
//
//  Created by 王峥 on 2019/6/26.
//  Copyright © 2019 futu. All rights reserved.
//

#import "ViewController.h"
#import <AFNetworking/AFNetworking.h>
#import <Foundation/Foundation.h>

@interface ViewController()
{
NSDictionary *jermaine;
    NSDictionary * data;
}
@property (weak) IBOutlet NSTextField *json;
@property (weak) IBOutlet NSTextField *defaultJson;
@property (weak) IBOutlet NSTextField *gcdGo;

//- (void)fetchCompletionBlock:(void (^ __nullable)(void))completion;
@end



@implementation ViewController

#pragma Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    AFHTTPSessionManager *manager =[AFHTTPSessionManager manager];
    //如过解析nsdata使用如下
//    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    //如过解析xml，使用解析如下
    //manager.responseSerializer = [AFXMLParserResponseSerializer serializer];
    NSDictionary *dict = @{
                           @"username":@"520it",
                           @"pwd":@"520it"
                           };
    // parameters 参数字典
    [manager GET:@"http://api.androidhive.info/volley/person_object.json" parameters:dict progress:nil  success:^(NSURLSessionDataTask *  task, id   responseObject) {
        //这里改变RunLoop模式
        CFRunLoopStop(CFRunLoopGetMain()); //使用runloop同步成功
        NSLog(@"拉取成功: responseObject = [%@]",responseObject);
        self->jermaine = responseObject;
        
        // task 我们可以通过task拿到响应头
        // responseObject:请求成功返回的响应结果（AFN内部已经把响应体转换为OC对象，通常是字典或数组)
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
       NSLog(@"拉取失败: responseObject = [%@]",error);
    }];
    //这里恢复RunLoop
    CFRunLoopRun();
    NSLog(@"%@嘿嘿嘿",jermaine);
}

- (void)viewDidAppear{
    NSLog(@"%@哈哈哈哈哈哈哈",jermaine);
}

#pragma Target Action

/*Processing JSON*/
- (IBAction)goMyfriend:(id)sender {
    NSLog(@"%@",jermaine[@"name"]);
    self.json.cell.title = jermaine[@"name"];
}

/*Processing Test for GCD Group*/
//同步失败
- (IBAction)goGcd:(id)sender {
    dispatch_semaphore_t sema = dispatch_semaphore_create(0);
    AFHTTPSessionManager *manager =[AFHTTPSessionManager manager];
    //创建组
    dispatch_group_t group = dispatch_group_create();
    //创建子线程队列
    dispatch_queue_t queue = dispatch_get_global_queue(0, 0);
    
    __block NSDictionary *duke;
    dispatch_group_async(group, queue, ^{
        [manager GET:@"http://api.androidhive.info/volley/person_object.json" parameters:nil progress:nil  success:^(NSURLSessionDataTask *  task, id   responseObject) {
            NSLog(@"拉取成功: responseObject = [%@]",responseObject);
            dispatch_semaphore_signal(sema);
            duke = responseObject;
            // task 我们可以通过task拿到响应头
            // responseObject:请求成功返回的响应结果（AFN内部已经把响应体转换为OC对象，通常是字典或数组)
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"拉取失败: responseObject = [%@]",error);
            dispatch_semaphore_signal(sema);
        }];
    });
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{ //原因出在这里呀啊啊啊啊啊啊啊啊啊啊啊啊啊啊queue就可以了
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
        NSLog(@"duke");
        NSLog(@"%@", duke[@"name"]);
        if(duke[@"name"]){
            self.gcdGo.cell.title = duke[@"name"];
        }
        else{
            self.gcdGo.cell.title = @"Haven't get the data yet";
        }
    });
}

/*Processing Default Situation*/
//同步失败
- (IBAction)defautsitaution:(id)sender {
    AFHTTPSessionManager *manager =[AFHTTPSessionManager manager];
    __block NSDictionary *duke;
    
    [manager GET:@"http://api.androidhive.info/volley/person_object.json" parameters:nil progress:nil  success:^(NSURLSessionDataTask *  task, id   responseObject) {
        NSLog(@"拉取成功: responseObject = [%@]",responseObject);
        duke = responseObject;
        // task 我们可以通过task拿到响应头
        // responseObject:请求成功返回的响应结果（AFN内部已经把响应体转换为OC对象，通常是字典或数组)
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"拉取失败: responseObject = [%@]",error);
    }];
    
    if(duke[@"name"]){
        self.defaultJson.cell.title = duke[@"name"];
    }
    else{
        self.defaultJson.cell.title = @"The data haven't been load yet";
    }
    
}

//dispatch_barrier_async方法任然失败
- (IBAction)hhh:(id)sender {
    dispatch_queue_t queue =  dispatch_queue_create(0, DISPATCH_QUEUE_CONCURRENT);
    AFHTTPSessionManager *manager =[AFHTTPSessionManager manager];
    __block NSData *duke;
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    dispatch_async(queue, ^{
        [manager GET:@"http://api.androidhive.info/volley/person_object.json" parameters:nil progress:nil  success:^(NSURLSessionDataTask *  task, id   responseObject) {
            //这里改变RunLoop模式
            NSLog(@"拉取成功: responseObject = [%@]",responseObject);
            duke = responseObject;
            // task 我们可以通过task拿到响应头
            // responseObject:请求成功返回的响应结果（AFN内部已经把响应体转换为OC对象，通常是字典或数组)
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"拉取失败: responseObject = [%@]",error);
        }];
    });
    dispatch_barrier_async(queue, ^{
        NSLog(@"拿到了duke的值了吗");
    });
    dispatch_async(queue, ^{
        NSLog(@"duke");
        NSString *cris = [[NSString alloc]initWithData:duke encoding:NSUTF8StringEncoding];
        NSLog(@"%@", cris);
    });

}

////使用AFN中的batchOfRequestOperations(在AF3被废弃），信号量方法
//- (IBAction)GoAFN:(id)sender {
//    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
//    dispatch_group_t group = dispatch_group_create();
//    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//
//
//    dispatch_group_async(group, queue, ^{
//        [self requestJSON:^{ //这个block是此网络任务异步请求结束时调用的,代表着网络请求的结束.
//            //一个任务结束时标记一个信号量
//            NSLog(@"duke嗯嗯");
//            NSLog(@"%@",[NSThread currentThread]);
//            dispatch_semaphore_signal(semaphore);
//        }];
//    });
//    dispatch_group_notify(group, queue, ^{
//        //1个任务,1个信号等待.
//        NSLog(@"之前的duke"); //发送请求后直接换到主线程，所以相当于queue中的线程都已经执行完毕，所以这里会输出duke
//        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
//        //这里就是所有异步任务请求结束后执行的代码
//        NSLog(@"之后的duke%@",self->frank);
//        NSString *cris = [[NSString alloc]initWithData:self->frank encoding:NSUTF8StringEncoding];
//        NSLog(@"哎呀呀呀%@", cris);
//
//
//    });
//
//}

//- (void)fetchCompletionBlock:(void (^ __nullable)(void))completion{
//    AFHTTPSessionManager *manager =[AFHTTPSessionManager manager];
//    __block NSDictionary *duke;
//
//    [manager GET:@"http://api.androidhive.info/volley/person_object.json" parameters:nil progress:nil  success:^(NSURLSessionDataTask *  task, id   responseObject) {
//        //这里改变RunLoop模式
//        CFRunLoopStop(CFRunLoopGetMain());
//        NSLog(@"拉取成功: responseObject = [%@]",responseObject);
//        duke = responseObject;
//        // task 我们可以通过task拿到响应头
//        // responseObject:请求成功返回的响应结果（AFN内部已经把响应体转换为OC对象，通常是字典或数组)
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        NSLog(@"拉取失败: responseObject = [%@]",error);
//    }];
//}

//-(void)requestJSON:(void (^ __nullable)(void))finish{
//    //网络请求..成功后调用一下finish,失败也调用finish
//    AFHTTPSessionManager *manager =[AFHTTPSessionManager manager];
//    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
//    [manager GET:@"http://api.androidhive.info/volley/person_object.json" parameters:nil progress:nil  success:^(NSURLSessionDataTask *  task, id   responseObject) {
//        NSLog(@"拉取成功: responseObject = [%@]",responseObject);
//        self->frank = responseObject;
//        NSLog(@"%@",[NSThread currentThread]);
//        // task 我们可以通过task拿到响应头
//        // responseObject:请求成功返回的响应结果（AFN内部已经把响应体转换为OC对象，通常是字典或数组)
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        NSLog(@"拉取失败: responseObject = [%@]",error);
//    }];
//    NSLog(@"SFSA");
////    NSLog(@"%@",[NSThread currentThread]);
////    finish(); //此时并不是在主线程上执行，因把该block转至主线程
//    dispatch_async(dispatch_get_main_queue(), finish);
//}

- (IBAction)rightVersion:(id)sender {
    dispatch_semaphore_t sem = dispatch_semaphore_create(0);
    AFHTTPSessionManager *manager =[AFHTTPSessionManager manager];
    dispatch_queue_t queue =  dispatch_queue_create(0, DISPATCH_QUEUE_CONCURRENT);
    __block NSDictionary *duke;
    dispatch_async(queue, ^{
        [manager GET:@"http://api.androidhive.info/volley/person_object.json" parameters:nil progress:nil  success:^(NSURLSessionDataTask *  task, id   responseObject) {
            NSLog(@"拉取成功: responseObject = [%@]",responseObject);
            duke = responseObject;
            dispatch_semaphore_signal(sem);
            // task 我们可以通过task拿到响应头
            // responseObject:请求成功返回的响应结果（AFN内部已经把响应体转换为OC对象，通常是字典或数组)
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"拉取失败: responseObject = [%@]",error);
            dispatch_semaphore_signal(sem);
        }];
    });
    dispatch_async(queue, ^{
        dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
        NSLog(@"duke");
        NSLog(@"%@", duke[@"name"]);
    });
}

//dispatch_semaphore_t hhh; //创建信息量的全局变量
//
//- (IBAction)leXiang:(id)sender {
//    dispatch_group_t group = dispatch_group_create();
//    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//    hhh = dispatch_semaphore_create(0); //初始化信息量为0
//
//    //加入信息量
//
//    dispatch_group_async(group, queue, ^{
//        [self requestJSON:^{ //这个block是此网络任务异步请求结束时调用的,代表着网络请求的结束.
//            NSLog(@"finsh block对应的thread%@",[NSThread currentThread]);
//        }];
//    });
//    dispatch_group_notify(group, queue, ^{
//
//        dispatch_semaphore_wait(hhh, DISPATCH_TIME_FOREVER);
//
//        NSLog(@"dispatch_group_notify中的代码对应的thread%@",[NSThread currentThread]);
//        if (self->data[@"name"]) {
//            NSLog(@"成功获取name: %@", self->data[@"name"]);
//        }
//        else{
//            NSLog(@"获取失败");
//        }
//    });
//
//}
//
//-(void)requestJSON:(void (^ __nullable)(void))finish{
//    //网络请求..成功后调用一下finish,失败也调用finish
//    AFHTTPSessionManager *manager =[AFHTTPSessionManager manager];
//    [manager GET:@"http://api.androidhive.info/volley/person_object.json" parameters:nil progress:nil  success:^(NSURLSessionDataTask *  task, id   responseObject) {
//        NSLog(@"拉取成功: responseObject = [%@]",responseObject);
//        self->data = responseObject;
//        NSLog(@"success block的thread%@",[NSThread currentThread]);
//
//        dispatch_semaphore_signal(hhh); //改变dispatch_semaphore_signal的位置
//
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        NSLog(@"拉取失败: responseObject = [%@]",error);
//        NSLog(@"fail block的thread%@",[NSThread currentThread]);
//
//        dispatch_semaphore_signal(hhh); //改变dispatch_semaphore_signal的位置
//
//    }];
//    NSLog(@"requestJSON block的thread%@",[NSThread currentThread]);
////    finish();
//    dispatch_async(dispatch_get_main_queue(), finish);
//}

- (IBAction)leXiang:(id)sender {
    AFHTTPSessionManager *manager =[AFHTTPSessionManager manager];
    __block NSString *data1;
    __block NSString *data2;
    
    
    
    [manager GET:@"http://ajax.googleapis.com/ajax/services/feed/load?q=http://www.bilibili.tv/rss-13.xml&v=1.0" parameters:nil progress:nil  success:^(NSURLSessionDataTask *  task, id   responseObject) {
        NSLog(@"拉取成功: responseObject = [%@]",responseObject);
        data1 = responseObject[@"responseDetails"]; //将拉取到的数据存取于data1中
        
        CFRunLoopStop(CFRunLoopGetMain()); //同时可将CFRunLoopGetMain替换为CFRunLoopGetCurrent，在完成前阻塞主进程
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"拉取失败: responseObject = [%@]",error);
    }];
    
    CFRunLoopRun(); //在完成前一任务后，继续执行主进程
    
    [manager GET:@"http://api.androidhive.info/volley/person_object.json" parameters:nil progress:nil  success:^(NSURLSessionDataTask *  task, id   responseObject) {
        NSLog(@"拉取成功: responseObject = [%@]",responseObject);
        data2 = responseObject[@"name"]; //将拉取到的数据存取于data2中
        
        CFRunLoopStop(CFRunLoopGetMain()); //同时可将CFRunLoopGetMain替换为CFRunLoopGetCurrent，在完成前阻塞主进程
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"拉取失败: responseObject = [%@]",error);
    }];
    
    CFRunLoopRun(); //在完成前一任务后，继续执行主进程
    NSLog(@"%@ Sorry about that, %@.", data1, data2);
}

@end
