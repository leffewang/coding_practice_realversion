//
//  PracticeForFMDBController.m
//  数据持久化练习
//
//  Created by 王峥 on 2019/6/25.
//  Copyright © 2019 futu. All rights reserved.
//

#import "PracticeForFMDBController.h"

@interface PracticeForFMDBController ()

@end

@implementation PracticeForFMDBController

#pragma Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    //获得沙盒文件下Documents路径
    NSString *documents = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    //打开数据库操作
    NSString *pathForDatabase = [documents stringByAppendingPathComponent:@"test.db"];
    FMDatabase *database = [FMDatabase databaseWithPath:pathForDatabase];
    //打开数据库，同时为数据库设置缓存,提高查询效率
    if(![database open]){
        NSLog(@"打不开，到底啊");
    }
    database.shouldCacheStatements = YES;
    
    //创建一个叫人类的表
    if (![database tableExists:@"Person"]) {
        
        [database executeUpdate:@"CREATE TABLE Person(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, age INTEGER)"];
        
        NSLog(@"创建表成功");
    }
    //增加数据操作
    NSNumber *IDNumber = @110;
    NSNumber *IDNumber1 = @120;
    NSNumber *IDNumber2 = @911;
    NSString *name = @"Jermaineeeeee";
    NSNumber *age = @18;
    [self addDataOperationID:IDNumber name:name age:age database:database];
    [self addDataOperationID:IDNumber1 name:name age:age database:database];
    [self addDataOperationID:IDNumber2 name:name age:age database:database];
    [self deleteDataOperationID:IDNumber database:database];
    [self updateDataOperationID:IDNumber1 name:@"Duke" age:@88 database:database];
    [self selectAllDataFromDatabase:database];
    [database close];
    
    
}

#pragma Private Method

//增加数据操作
- (void)addDataOperationID:(NSNumber *)IDNumber name:(NSString *)name age:(NSNumber *)age  database: (FMDatabase *)database{
    
    [database executeUpdate:@" INSERT INTO Person (id,name,age) VALUES (?,?,?)",IDNumber,name,age];
    
}

//删除数据操作
- (void)deleteDataOperationID:(NSNumber *)IDNumebr database: (FMDatabase *)database{
    [database executeUpdate:@" DELETE FROM Person WHERE id = ?", IDNumebr];
}

//更新数据操作
- (void)updateDataOperationID:(NSNumber *)IDNumber name:(NSString *)name age:(NSNumber *)age  database: (FMDatabase *)database{
    [database executeUpdate:@" UPDATE Person SET name = ? WHERE id = ?",name , IDNumber];
    [database executeUpdate:@" UPDATE Person SET age = ? WHERE id = ?",age , IDNumber];
}

//查询数据操作

- (void)selectAllDataFromDatabase: (FMDatabase *)database{
    FMResultSet * resultSet = [database executeQuery:@" SELECT * FROM Person"];
    while ([resultSet next]) {
        
        int Id = [resultSet intForColumn:@"id"];
        NSString * name = [resultSet stringForColumn:@"name"];
        int age = [resultSet intForColumn:@"age"];
        
        NSLog(@"personId:%i,personName:%@,personAge:%i",Id,name,age);
    }
}
@end
