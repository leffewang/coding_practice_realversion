//
//  ViewController.m
//  数据持久化练习
//
//  Created by 王峥 on 2019/6/24.
//  Copyright © 2019 futu. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController

#pragma Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    /*NSUserDefaults练习*/
    NSURL *url = [NSURL URLWithString:@"http://dota2dbpic.uuu9.com/255b198c-2a79-4376-b08f-b3a1783604ae00.jpg"];
    NSUserDefaults *jermaine = [NSUserDefaults standardUserDefaults];
    [jermaine setURL:url forKey:@"HiJermaine"];
    [jermaine synchronize];
    NSString *duke = [(NSURL *)[jermaine URLForKey:@"HiJermaine"] absoluteString];
    NSLog(@"出现把网址%@", duke);
    
    /*plist练习*/
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    //获取完整路径
    NSString *documentsPath = [path objectAtIndex:0];
    NSString *plistPath = [documentsPath stringByAppendingPathComponent:@"MY_PropertyList.plist"];
    
    NSMutableDictionary *usersDic = [[NSMutableDictionary alloc ] init];
    //设置属性值
    [usersDic setObject:@"Jermiane" forKey:@"name"];
    [usersDic setObject:@"manWithGlory" forKey:@"password"];
    //写入文件
    [usersDic writeToFile:plistPath atomically:YES];
    
    //以上为代码创建部分储存在沙盒中，之后为手动在项目中创建部分,修改读写
    NSString *crisPath = [[NSBundle mainBundle]pathForResource:@"hiDuke" ofType:@"plist"];
    NSMutableDictionary *cris = [[NSMutableDictionary alloc]initWithContentsOfFile:crisPath];
    [cris setObject:@"Duke" forKey:@"name"];
    [cris setObject:@"Sleeping" forKey:@"hobby"];
    [cris writeToFile:crisPath atomically:YES];
    NSDictionary *jermainee = [[NSDictionary alloc]initWithContentsOfFile:crisPath];
    NSLog(@"====%@",jermainee);
    //写入失败，bundle中只能读，需要移出或另存（如果需要写入的话）
    
    
    /*沙盒目录存取并读取图片*/
    NSArray *newPath = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *imagePath = [newPath objectAtIndex:0];
    NSData *image = [[NSData alloc]initWithContentsOfURL:url];
    NSString *filePath = [imagePath stringByAppendingPathComponent:@"testFile.png"];
    [image writeToFile:filePath atomically:YES];
}


- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}


@end
