//
//  ViewController.m
//  多线程GCD
//
//  Created by 王峥 on 2019/6/24.
//  Copyright © 2019 futu. All rights reserved.
//

#import "ViewController.h"
@interface ViewController ()
@property (weak) IBOutlet NSImageView *picture;
@end

@implementation ViewController
dispatch_queue_t serialQueue;
dispatch_queue_t concurrentQueue;

#pragma Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    //串行队列
    serialQueue = dispatch_queue_create("fkjava.queue", DISPATCH_QUEUE_SERIAL);
    //并发队列
    concurrentQueue = dispatch_queue_create("fkjava.queue", DISPATCH_QUEUE_CONCURRENT);
}

#pragma Private Method
- (IBAction)goSlark:(id)sender {
    dispatch_async(serialQueue, ^(void){
        for (int i = 0; i < 100; i++){
            NSLog(@"%@=====%d",[NSThread currentThread],i);
        }
    });
    
    dispatch_async(serialQueue, ^(void){
        for (int i = 0; i < 100; i++){
            NSLog(@"%@=====%d",[NSThread currentThread],i);
        }
    });
}

//只有这个不一，只有这个异步执行
- (IBAction)goMiracle:(id)sender {
    dispatch_async(concurrentQueue, ^(void){
        for (int i = 0; i < 100; i++){
            NSLog(@"%@=====%d",[NSThread currentThread],i);
        }
    });
    
    dispatch_async(concurrentQueue, ^(void){
        for (int i = 0; i < 100; i++){
            NSLog(@"%@=====%d",[NSThread currentThread],i);
        }
    });
}

- (IBAction)special:(id)sender {
    dispatch_sync(serialQueue, ^(void){
        for (int i = 0; i < 100; i++){
            NSLog(@"%@=====%d",[NSThread currentThread],i);
        }
    });
    
    dispatch_sync(serialQueue, ^(void){
        for (int i = 0; i < 100; i++){
            NSLog(@"%@=====%d",[NSThread currentThread],i);
        }
    });
}
- (IBAction)goJermiane:(id)sender {
    dispatch_sync(concurrentQueue, ^(void){
        for (int i = 0; i < 10000; i++){
            NSLog(@"%@=====%d",[NSThread currentThread],i);
        }
    });
    
    dispatch_sync(concurrentQueue, ^(void){
        for (int i = 0; i < 10000; i++){
            NSLog(@"%@=====%d",[NSThread currentThread],i);
        }
    });
}


- (IBAction)showPicture:(id)sender {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^(void){
        NSURL *url = [NSURL URLWithString:@"http://dota2dbpic.uuu9.com/255b198c-2a79-4376-b08f-b3a1783604ae00.jpg"];
        NSImage *image = [[NSImage alloc]initWithContentsOfURL:url];
        if(image != nil){
            dispatch_async(dispatch_get_main_queue(), ^(void){
                self.picture.image = image;
            });
        }
        else{
            NSLog(@"There should be some mistakes.");
        }
    });
}

- (IBAction)goLiquid:(id)sender {
    dispatch_apply(10, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(size_t time){
        NSLog(@"====冲鸭第 %lu 次====%@", time, [NSThread currentThread]);
    });
}

- (IBAction)goDuke:(id)sender {
    static dispatch_once_t onceToken;
    //主线程直接执行
    dispatch_once(&onceToken, ^(void){
        NSLog(@"==执行代码块==");
        [NSThread sleepForTimeInterval:3];
    });
}
@end
