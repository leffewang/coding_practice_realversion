//
//  cancelThreadController.m
//  多线程补
//
//  Created by 王峥 on 2019/6/21.
//  Copyright © 2019 futu. All rights reserved.
//

#import "cancelThreadController.h"

@interface cancelThreadController ()

@end

@implementation cancelThreadController
NSThread *thread;

- (void)viewDidLoad {
    [super viewDidLoad];
    //创建新线程对象
    thread = [[NSThread alloc] initWithTarget:self selector:@selector(run) object:nil];
    //冲鸭
    [thread start];
}

- (void)run{
    for (int i = 0; i < 100; i++){
        if([NSThread currentThread].isCancelled){
            [NSThread exit];
        }
        NSLog(@"--rock----%@---%d",[NSThread currentThread].name, i);
        [NSThread sleepForTimeInterval:0.5];
    }
}

- (IBAction)cancelThread:(id)sender{
    [thread cancel];
}
@end
