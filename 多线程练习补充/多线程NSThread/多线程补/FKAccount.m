//
//  FKAccount.m
//  多线程补
//
//  Created by 王峥 on 2019/6/24.
//  Copyright © 2019 futu. All rights reserved.
//

#import "FKAccount.h"

@implementation FKAccount
//NSLock *lock;
//
//- (instancetype)init{
//    self = [super init];
//    if (self){
//        lock = [[NSLock alloc] init]; //初始化lock
//    }
//    return self;
//}

//NSCondition lock
NSCondition *cond;
BOOL flag = YES;
- (instancetype) init{
    self = [super init];
    if (self){
        cond = [[NSCondition alloc] init];
    }
    return self;
}

- (instancetype)initWithAccountNo: (NSString *) accountNo balance: (CGFloat) balance{
    self = [super init];
    if (self) {
//        lock = [[NSLock alloc] init];  //初始化lock
        cond = [[NSCondition alloc] init];
        _accountNo = accountNo;
        _balance = balance;
    }
    return self;
}

- (void)draw:(CGFloat)drawAmount{
    //加锁
//    [lock lock];
    //添加同步监视器  不需要lock即可
    //@synchronized (self) {
    
    [cond lock];
    if (!flag){ //存钱状态堵塞
        [cond wait];
    }
    else{
//        if (self.balance >= drawAmount){
//            NSLog(@"%@取钱成功!吐出钞票：%g",[NSThread currentThread].name, drawAmount);
//            //[NSThread sleepForTimeInterval:0.0001];
//            _balance = _balance - drawAmount;
//            NSLog(@"\t余额为：%g", self.balance);
//        }
//        else{
//            NSLog(@"%@取钱失败，再见了您", [NSThread currentThread].name);
        //下面该方法是给NSCondition用的
        NSLog(@"%@取钱：%g",[NSThread currentThread], drawAmount);
        _balance -= drawAmount;
        NSLog(@"\t余额为：%g", self.balance);
        flag = NO;
        [cond broadcast];
    }
    [cond unlock];
    //}
    //解锁
//    [lock unlock];
}

- (void)deposit:(CGFloat) depositAmount{
    [cond lock];
    if(flag){
        [cond wait];
    }
    else{
        NSLog(@"%@存钱：%g",[NSThread currentThread], depositAmount);
        _balance += depositAmount;
        NSLog(@"\t余额为：%g", self.balance);
        flag = YES;
        [cond broadcast];
    }
    [cond unlock];
}

- (NSUInteger)hash{
    return [self.accountNo hash];
}

- (BOOL)isEqual:(id)object{
    if(self == object){
        return YES;
    }
    else if(object != nil && [object class] == [FKAccount class]){
        FKAccount *target = (FKAccount *) object;
        return [target.accountNo isEqualToString:self.accountNo];
    }
    return NO;
}
@end
