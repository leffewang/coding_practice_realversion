//
//  SecurityTestController.m
//  多线程补
//
//  Created by 王峥 on 2019/6/24.
//  Copyright © 2019 futu. All rights reserved.
//

#import "SecurityTestController.h"

@interface SecurityTestController ()

@end

@implementation SecurityTestController

FKAccount *account;

#pragma Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    account = [[FKAccount alloc]initWithAccountNo:@"jermaine" balance:25000];
}

#pragma Private Method

- (IBAction)draw:(id)sender{
    NSThread *thread1 = [[NSThread alloc]initWithTarget:self selector:@selector(drawMethod:) object:[NSNumber numberWithInt:8000]];
    
    NSThread *thread2 = [[NSThread alloc]initWithTarget:self selector:@selector(drawMethod:) object:[NSNumber numberWithInt:8000]];
    
    NSThread *thread3 = [[NSThread alloc]initWithTarget:self selector:@selector(depositMethod:) object:[NSNumber numberWithInt:8000]];
    
    [thread1 start];
    [thread2 start];
    [thread3 start];
    
}

- (void)drawMethod: (NSNumber *)drawAmout{
    [account draw:drawAmout.doubleValue];
}

- (void)depositMethod: (NSNumber *)depositAmout{
    [account deposit:depositAmout.doubleValue];
}
@end
