//
//  DownloadController.m
//  多线程补
//
//  Created by 王峥 on 2019/6/21.
//  Copyright © 2019 futu. All rights reserved.
//

#import "DownloadController.h"

@interface DownloadController ()
@property (weak) IBOutlet NSImageView *picture;

@end

@implementation DownloadController

#pragma Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
}

#pragma prviate method
- (IBAction)showPicture:(id)sender {
    NSString *url = @"http://dota2dbpic.uuu9.com/92a8eaa6-7141-4300-bca9-01d02f30e453tk.png";
    NSThread *thread = [[NSThread alloc]initWithTarget:self selector:@selector(downloadImageFromURL:) object:url];
    [thread start];
    
    //创建第一个线程对象。
    NSThread *thread1 = [[NSThread alloc]initWithTarget:self selector:@selector(run) object:nil];
    thread1.name = @"线程A";
    NSLog(@"线程A的优先级为：%g", thread1.threadPriority);
    thread1.threadPriority = 0.0;
    
    //创建第二个线程对象。
    NSThread *thread2 = [[NSThread alloc]initWithTarget:self selector:@selector(run) object:nil];
    thread2.name = @"线程B";
    NSLog(@"线程B的优先级为：%g", thread2.threadPriority);
    thread2.threadPriority = 1.0;
    
    [thread1 start];
    [thread2 start];
}

- (void)downloadImageFromURL: (NSString *) url{
    NSData *data = [[NSData alloc]initWithContentsOfURL:[NSURL URLWithString:url]];
    NSImage *image = [[NSImage alloc]initWithData:data];
    if(image != nil){
        [self performSelectorOnMainThread:@selector(updateView:) withObject:image waitUntilDone:YES];
    }
    else{
        NSLog(@"There should be some mistakes.");
    }
}

- (void)updateView: (NSImage *)image{
    self.picture.image = image;
    NSLog(@"Finsh");
}

- (void)run{
    for (int i = 0; i < 10000; i++){
        NSLog(@"------%@---%d",[NSThread currentThread].name, i);
    }
}

@end
