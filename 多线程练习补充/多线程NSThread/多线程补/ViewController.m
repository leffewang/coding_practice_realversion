//
//  ViewController.m
//  多线程补
//
//  Created by 王峥 on 2019/6/21.
//  Copyright © 2019 futu. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController

#pragma mark Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    for(int i = 0; i < 100; i++){
        NSLog(@"===%@===%d", [NSThread currentThread], i);
        if(i == 20){
            //创建线程
            NSThread *thread = [[NSThread alloc]initWithTarget:self selector:@selector(run) object:nil];
            //冲鸭
            [thread start];
            //创建并启动新线程
            [NSThread detachNewThreadSelector:@selector(run) toTarget:self withObject:nil];
            
        }
    }
    // Do any additional setup after loading the view.
}


- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}

- (void)run{
    for (int i = 0; i < 1000; i++){
        NSLog(@"------%@---%d",[NSThread currentThread], i);
    }
}

@end
