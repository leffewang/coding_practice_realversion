//
//  FKAccount.h
//  多线程补
//
//  Created by 王峥 on 2019/6/24.
//  Copyright © 2019 futu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FKAccount : NSObject

@property (nonatomic, copy) NSString *accountNo;
@property (nonatomic, readonly) CGFloat balance;

- (instancetype)initWithAccountNo: (NSString *) accountNo balance: (CGFloat) balance;

- (void)draw: (CGFloat) drawAmount;

- (void)deposit:(CGFloat) depositAmount;
@end
