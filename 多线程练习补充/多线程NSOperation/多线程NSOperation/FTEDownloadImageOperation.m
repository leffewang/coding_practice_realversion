//
//  FTEDownloadImageOperation.m
//  多线程NSOperation
//
//  Created by 王峥 on 2019/6/24.
//  Copyright © 2019 futu. All rights reserved.
//

#import "FTEDownloadImageOperation.h"

@implementation FTEDownloadImageOperation

- (instancetype)initWithURL:(NSURL *)url imageView:(NSImageView *)centerView{
    self = [super init];
    if (self){
        _centerView = centerView;
        _url = url;
    }
    return self;
}

//必须重写main方法，当继承nsoperation时
- (void) main{
    NSImage *image = [[NSImage alloc]initWithContentsOfURL:self.url];
    if(image != nil){
        [self performSelectorOnMainThread:@selector(updateUI:) withObject:image waitUntilDone:YES];
    }
    else{
        NSLog(@"到底啊");
    }
}

- (void)updateUI: (NSImage *)image{
    self.centerView.image = image;
}

@end
