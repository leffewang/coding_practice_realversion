//
//  FTEDownloadImageOperation.h
//  多线程NSOperation
//
//  Created by 王峥 on 2019/6/24.
//  Copyright © 2019 futu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>


@interface FTEDownloadImageOperation : NSOperation
@property (nonatomic, strong) NSURL *url;
@property (nonatomic, weak)  NSImageView *centerView;

- (instancetype)initWithURL:(NSURL *)url imageView:(NSImageView *)centerView;
@end

