//
//  ViewController.m
//  多线程NSOperation
//
//  Created by 王峥 on 2019/6/24.
//  Copyright © 2019 futu. All rights reserved.
//

#import "ViewController.h"
#import "FTEDownloadImageOperation.h"

@interface ViewController ()
@property (weak) IBOutlet NSImageView *picture;

@end

@implementation ViewController
NSOperationQueue *queue;

#pragma Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    queue = [[NSOperationQueue alloc]init];
    
    queue.maxConcurrentOperationCount = 10l;
}

#pragma Private Method
- (IBAction)ComeOn:(id)sender {
    NSURL *url = [NSURL URLWithString:@"http://dota2dbpic.uuu9.com/a4b83db4-6182-4ca4-a4ad-45e7d8612819lich.gif"];
    NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:^(void){
        NSImage *image = [[NSImage alloc]initWithContentsOfURL:url];
        if(image != nil){
            [self performSelectorOnMainThread:@selector(updateUI:) withObject:image waitUntilDone:YES];
        }
        else{
            NSLog(@"到底啊");
        }
    }];
    [queue addOperation:operation];
}

- (void)updateUI: (NSImage *)image{
    self.picture.image = image;
}

- (IBAction)goFrank:(id)sender {
    NSURL *url = [NSURL URLWithString:@"http://dota2dbpic.uuu9.com/793a1a57-0fea-4395-aa86-5add327b337fwd.gif"];
    FTEDownloadImageOperation *operation = [[FTEDownloadImageOperation alloc]initWithURL:url imageView:self.picture];
    [queue addOperation:operation];
    
    
    
}
@end
