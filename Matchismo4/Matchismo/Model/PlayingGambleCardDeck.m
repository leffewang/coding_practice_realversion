//
//  PlayingGambleCardDeck.m
//  Matchismo
//
//  Created by 王峥 on 2019/5/27.
//  Copyright © 2019 futu. All rights reserved.
//

#import "PlayingGambleCardDeck.h"

@implementation PlayingGambleCardDeck


- (instancetype)init
{
    self = [super init];
    
    if(self){
        for (NSString *suit in [PlayingGambleCard gambleValidSuits]){
            for (NSUInteger rank = 1; rank <= [PlayingGambleCard gambleMaxRank]; rank++){
                PlayingGambleCard *card = [[PlayingGambleCard alloc] init];
                card.gambleRank = rank;
                card.gambleSuit = suit;
                [self addCard:card];
            }
        }
    }
    return self;
}

@end
