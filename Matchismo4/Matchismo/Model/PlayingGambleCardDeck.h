//
//  PlayingGambleCardDeck.h
//  Matchismo
//
//  Created by 王峥 on 2019/5/27.
//  Copyright © 2019 futu. All rights reserved.
//

#import "Deck.h"
#import "PlayingGambleCard.h"

NS_ASSUME_NONNULL_BEGIN

@interface PlayingGambleCardDeck : Deck

@end

NS_ASSUME_NONNULL_END
