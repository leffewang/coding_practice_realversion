//
//  PlayingCard.h
//  Matchismo
//
//  Created by 王峥 on 2019/5/25.
//  Copyright © 2019 futu. All rights reserved.
//

#import "Card.h"

/*The parent class of Playing card is Card*/
@interface PlayingGambleCard : Card
/*The suit means the type of card*/
@property (strong, nonatomic) NSString *gambleSuit;
/*The rank means the value of card*/
@property (nonatomic) NSUInteger gambleRank;

/*return an array comprise the valid suits*/
+ (NSArray *)gambleValidSuits;
/*return an interger which is the max rank*/
+ (NSUInteger)gambleMaxRank;
@end
