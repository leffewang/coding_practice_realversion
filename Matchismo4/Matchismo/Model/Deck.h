//
//  Deck.h
//  Matchismo
//
//  Created by 王峥 on 2019/5/22.
//  Copyright © 2019 futu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Card.h"


@interface Deck : NSObject
/*Add card at top or not*/
-(void)addCard: (Card *) card atTop:(BOOL)atTop;

/*Add card directly at the end*/
-(void)addCard: (Card *) card;

/*Draw random card from deck*/
-(Card *)drawRandomCard;

@end
