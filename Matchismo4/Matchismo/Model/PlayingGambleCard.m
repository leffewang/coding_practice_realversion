//
//  PlayingCard.m
//  Matchismo
//
//  Created by 王峥 on 2019/5/25.
//  Copyright © 2019 futu. All rights reserved.
//

#import "PlayingGambleCard.h"

@implementation PlayingGambleCard
- (NSString*)contents{
    NSArray *rankString = [PlayingGambleCard gambleRankStrings];
    return [rankString[self.gambleRank] stringByAppendingString:self.gambleSuit];
}

+ (NSArray *)gambleValidSuits{
    NSArray *suits = [[NSArray alloc]init];
    suits = @[@"🔺",@"🔴",@"◽️"];
    return suits;
}

+ (NSUInteger)gambleMaxRank{
    return [[self gambleRankStrings] count] -1;
}

+ (NSArray *) gambleRankStrings{
    NSArray *rankstring = [[NSArray alloc]init];
    rankstring = @[@"?",@"A",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"J",@"Q",@"K"];
    return rankstring;
}

@synthesize gambleSuit = _gambleSuit;

- (NSString *)gambleSuit{
    return _gambleSuit ? _gambleSuit: @"?";
}

- (void)setSuit:(NSString *)gambleSuit
{
    if([[PlayingGambleCard gambleValidSuits] containsObject:gambleSuit]){
        _gambleSuit = gambleSuit;
    }
}

- (void) setRank:(NSUInteger)gambleRank
{
    if(gambleRank <= [PlayingGambleCard gambleMaxRank]){
        _gambleRank = gambleRank;
    }
}

-(int)gamblematch:(NSArray *)otherCards{
    int score = 0;
    if ([otherCards count] == 1){
        id  otherCard = [otherCards firstObject];
        if ([otherCard isKindOfClass:[PlayingGambleCard class]]){
            PlayingGambleCard * otherCardRight = (PlayingGambleCard *)otherCard;
            if (otherCardRight.gambleSuit == self.gambleSuit && otherCardRight.gambleRank < self.gambleRank){
                score = 4;
            }
            else if (otherCardRight.gambleSuit != self.gambleSuit && otherCardRight.gambleRank < self.gambleRank){
                score = 1;
            }
            else if (otherCardRight.gambleRank > self.gambleRank){
                score = -1;
            }
        }
    }
    return score;
}
@end

