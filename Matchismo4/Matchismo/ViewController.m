//
//  ViewController.m
//  Matchismo
//
//  Created by 王峥 on 2019/5/22.
//  Copyright © 2019 futu. All rights reserved.
//

#import "ViewController.h"
#import "CardMatchingGame.h"

@interface ViewController ()
@property (strong, nonatomic) Deck * cardlist;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *cardButtons;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (strong, nonatomic) CardMatchingGame *game;
@property (weak, nonatomic) IBOutlet UISegmentedControl *ModeSegementControl;
@end

@implementation ViewController

-(Deck *)createDeck{
    return [[PlayingCardDeck alloc]init];
}

-(CardMatchingGame *)game{
    if(!_game) _game = [[CardMatchingGame alloc]initWithCardCount:30 usingDeck:[self createDeck]];
    return _game;
}

- (IBAction)touchCardButton:(UIButton *)sender
{
    /*Change to the hind of card if there existing contents in current title*/
    if(self.ModeSegementControl.selectedSegmentIndex == 0){
        int chosenButtonIndex = (int)[self.cardButtons indexOfObject:sender];
        [self.game chooseCardAtIndex:chosenButtonIndex];
        [self updateUI];
    }
    else if(self.ModeSegementControl.selectedSegmentIndex == 1){
        int chosenButtonIndex = (int)[self.cardButtons indexOfObject:sender];
        [self.game chooseCardAtIndexInThreeCardMode:chosenButtonIndex];
        [self updateUI];
    }
}

-(void)updateUI{
    for (UIButton *cardButton in self.cardButtons){
        int cardButtonIndex = (int)[self.cardButtons indexOfObject:cardButton];
        Card *card = [self.game cardAtIndex:cardButtonIndex];
        [cardButton setTitle:[self tittleForCard:card] forState:UIControlStateNormal];
        [cardButton setBackgroundImage:[self backgroundImageForCard:card] forState:UIControlStateNormal];
        cardButton.enabled = !card.isMatched;
        self.scoreLabel.text = [NSString stringWithFormat:@"Score: %ld", (long)self.game.score];
    }
    if(self.game.score != 0){
        self.ModeSegementControl.enabled = false;
    }
}

-(NSString *)tittleForCard:(Card *)card{
    return card.isChosen ? card.contents : @"";
}

-(UIImage *)backgroundImageForCard:(Card *)card{
    return [UIImage imageNamed:card.isChosen ? @"cardfront" : @"cardback"];
}

- (IBAction)RestartButton:(UIButton *)sender {
    self.ModeSegementControl.enabled = true;
    self.game = [[CardMatchingGame alloc]initWithCardCount:30 usingDeck:[self createDeck]];
    [self updateUI];
}

@end
