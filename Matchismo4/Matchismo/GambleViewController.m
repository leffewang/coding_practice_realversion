//
//  GambleViewController.m
//  Matchismo
//
//  Created by 王峥 on 2019/5/27.
//  Copyright © 2019 futu. All rights reserved.
//

#import "GambleViewController.h"
#import "CardMatchingGame.h"
#import "PlayingGambleCardDeck.h"

@interface GambleViewController ()
@property (strong, nonatomic) Deck * cardlist;
@property (weak, nonatomic) IBOutlet UILabel *gambleScore;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *GambleCardCollections;
@property (strong, nonatomic) CardMatchingGame *game;
@end

@implementation GambleViewController

-(Deck *)createDeck{
    return [[PlayingGambleCardDeck alloc]init];
}



-(CardMatchingGame *)game{
    if(!_game) _game = [[CardMatchingGame alloc]initWithCardCount:6 usingDeck:[self createDeck]];
    return _game;
}
- (IBAction)restartGambleGame:(UIButton *)sender {
    self.game = [[CardMatchingGame alloc]initWithCardCount:6 usingDeck:[self createDeck]];
    [self updateUI];
}

- (IBAction)GambleClickButton:(UIButton *)sender {
    int chosenButtonIndex = (int)[self.GambleCardCollections indexOfObject:sender];
    NSLog(@"%d",chosenButtonIndex);
    [self.game chooseGambleCardAtIndex:chosenButtonIndex];
    [self updateUI];
}
- (void)updateUI{
    for (UIButton *cardButton in self.GambleCardCollections){
        int cardButtonIndex = (int)[self.GambleCardCollections indexOfObject:cardButton];
        Card *card = [self.game cardAtIndex:cardButtonIndex];
        [cardButton setTitle:[self tittleForCard:card] forState:UIControlStateNormal];
        [cardButton setBackgroundImage:[self backgroundImageForCard:card] forState:UIControlStateNormal];
        cardButton.enabled = !card.isMatched;
        self.gambleScore.text = [NSString stringWithFormat:@"Score: %ld", (long)self.game.score];
    }
}
-(NSString *)tittleForCard:(Card *)card{
    return card.isChosen ? card.contents : @"";
}

-(UIImage *)backgroundImageForCard:(Card *)card{
    return [UIImage imageNamed:card.isChosen ? @"cardfront" : @"cardback"];
}
@end
