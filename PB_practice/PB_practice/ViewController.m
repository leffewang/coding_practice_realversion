//
//  ViewController.m
//  PB_practice
//
//  Created by 王峥 on 2019/6/25.
//  Copyright © 2019 futu. All rights reserved.
//

#import "ViewController.h"
#import "Jermaine.pbobjc.h"

@implementation ViewController

#pragma Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    jermaine *duke = [[jermaine alloc]init];
    duke.age = 100;
    duke.name = @"Cris";
    duke.gender = @"girl";
    
    NSData *data =  [duke data];
    
    jermaine *frank = [jermaine parseFromData:data error:nil];
    NSLog(@"出来吧duke:%@", frank);
    // Do any additional setup after loading the view.
}

@end
